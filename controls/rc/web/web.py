from sensors import camera
from config import Config


class Web:
    control = None  # set externally before instantiation
    # FIXME: suppress double-click behaviour
    CSS = '''<style>
    table { border: 1px solid black; }
    tr {-webkit-user-select: none; }
    #centeredTDs td { text-align: center; vertical-align: middle; }
    </style>'''
    META = '<meta http-equiv="refresh" content="5">'
    JS = '''<script>
      function mousedown(id) {{ var xhr = new XMLHttpRequest(); xhr.open("POST", "", true); xhr.send(id+"=PRESSED"); }}
      function mouseup(id)   {{ var xhr = new XMLHttpRequest(); xhr.open("POST", "", true); xhr.send(id+"=RELEASED"); }}
    </script>'''

    @staticmethod
    def components(path) -> str:
        return f'''
<table>
<tr><th colspan="2"><a href="{'COMPONENTS' if path == '/' else '/'}">COMPONENTS</a></th></tr>
{''.join([f'<tr>'
          f'<td><div type="button" id="{c.__class__.__name__}" onmousedown="mousedown(id)">{c.__class__.__name__}</div></td>'
          f'<td><div type="button" id="{c.__class__.__name__}" onmousedown="mousedown(id)">{c.state}</div></td>' +
          "".join([f'<td><input type="submit" id="{value.__name__}" onmousedown="mousedown(id)" value="{key}"/></td>' for key, value in c.actions.items()]) +
          f'</tr>' for c in Web.control.components])}
<tr><td>{Web.control.__class__.__name__}</td><td>{Web.control.state}</td></tr>
</table>'''

    @staticmethod
    def rc(path: str, server_address: tuple) -> str:
        # FIXME: center the table
        return f'''
<table id="centeredTDs">
  <tr><th colspan="7"><a href="{'RC' if path == '/' else '/'}">RC</a></th></tr>

  <tr><td></td>
      <td><input type='submit' id="LookLeftUp"          onmousedown="mousedown(id)" value="⌜"/>
          <input type='submit' id="LookCenterUp"        onmousedown="mousedown(id)" value="⎺">
          <input type='submit' id="LookRightUp"         onmousedown="mousedown(id)" value="⌝"/></td>
      <td></td></tr>

  <tr><td></td>
      <td><input type='submit' id="LookLeftCenter"      onmousedown="mousedown(id)" value="⎸"/>
          <input type='submit' id="LookCenterCenter"    onmousedown="mousedown(id)" value="·">
          <input type='submit' id="LookRightCenter"     onmousedown="mousedown(id)" value="⎹"/></td>
      <td></td></tr>

  <tr><td></td>
      <td><input type='submit' id="LookLeftDown"        onmousedown="mousedown(id)" value="⌞"/>
          <input type='submit' id="LookCenterDown"      onmousedown="mousedown(id)" value="⎽">
          <input type='submit' id="LookRightDown"       onmousedown="mousedown(id)" value="⌟"/></td>
      <td></td></tr>

  <tr><td><input type='submit' id="SpeedUp"     onmousedown="mousedown(id)" value="⏫"/></td>
      <td><input type='submit' id="TurnCenter"  onmousedown="mousedown(id)" value="↔"/ style="width:640px"></td>
      <td><input type='submit' id="SpeedUp"     onmousedown="mousedown(id)" value="⏫"/></td></tr>

  <tr><td><input type='submit' id="TurnLeft"    onmousedown="mousedown(id)" value="◀" style="height:480px"/></td>
      <td><iframe width="640" height="480" src="http://{server_address[0]}:{server_address[1]+1}/stream.mjpg"></iframe></td>
      <td><input type='submit' id="TurnRight"   onmousedown="mousedown(id)" value="▶" style="height:480px"/></td></tr>

  <tr><td><input type='submit' id="SpeedDown"   onmousedown="mousedown(id)" value="⏬"/></td>
      <td><input type='submit' id="Stop"        onmousedown="mousedown(id)" value="⏹" style="width:640px"/></td>
      <td><input type='submit' id="SpeedDown"   onmousedown="mousedown(id)" value="⏬"/></td></tr>
</table>
'''

    @staticmethod
    def home_page(path, server_address) -> str:
        return f"""
<html><head>{Web.CSS}{Web.META}<title>Remote Control</title></head>
<body>
{Web.JS}
{Web.components(path)}
{Web.rc(path, server_address)}
<p>TODO: CONSOLE LOG</p>
</body></html>
"""

    @staticmethod
    def page(path: str, server_address: tuple) -> str:
        sub_page = path.rsplit('/', maxsplit=1)[-1]
        return {
            '': Web.home_page(path, server_address),
            'COMPONENTS': f'<html><head>{Web.META}<title>COMPONENTS</title></head><body>{Web.JS}{Web.components(path)}</body></html>',
            'RC': f'<html><head>{Web.CSS}<title>RC</title></head><body>{Web.JS}{Web.rc(path, server_address)}</body></html>',
            'CAMERA': f'<html><head><style>body{{background: url("camera_feed") no-repeat center;}}</style></head><body></body></html>'  # FIXME: move to iframe to allow refresh COMPONENT web again
        }[sub_page]

    @staticmethod
    def image(path) -> any:
        image_name = path.rsplit('/', maxsplit=1)[-1]
        if image_name == 'camera_feed':
            return Web.camera_image
        else:
            Web.control.rc.logger.error(f'Unknown {image_name} requested')
            return None

    @property
    def camera_image(self) -> any:
        cam = next(c for c in self.control.components if isinstance(c, camera.Camera))
        return cam.jpg()
