import logging
from http.server import BaseHTTPRequestHandler

from lib.utils import who
from controls.rc.web import Web


logger = logging.getLogger(__name__)


class RootWebHandler(BaseHTTPRequestHandler):
    actions_kwargs = {'duration': 0.1, 'priority': 1000, 'justification': 'Direct Web Action.', 'same_actions_limit': 5, 'abort_previous': True}
    request_timeout = 0.1  # Workaround for https://github.com/python/cpython/issues/99777 with possible interrupted page reload site-effect

    def setup(self):
        super().setup()
        self.request.settimeout(self.request_timeout)

    def get_content(self) -> (int, str, any):
        try:
            try:
                return 200, 'text/html;charset=utf-8', bytes(Web.page(self.path, self.server.server_address), 'utf-8')
            except KeyError:
                return 200, 'image/jpg', Web.image(self.path)
        except Exception as ex:
            return 500, 'text/html;charset=utf-8', bytes(f'<h1>{self.__class__.__name__} has thrown an exception:</h1>{ex}', 'utf-8')

    def _send_headers(self, code: int, content_type: str, content_length: int) -> None:
        self.send_response(code)
        self.send_header('Content-type', content_type)
        self.send_header('Content-length', str(content_length))
        self.end_headers()

    def do_GET(self):
        code, content_type, content = self.get_content()
        self._send_headers(code, content_type, len(content))
        self.wfile.write(content)

    def do_POST(self):
        post_data = self.rfile.read(int(self.headers.get('Content-Length'))).decode('utf-8')
        data = post_data.split('=', 1)[0]

        try:
            Web.control.perform_action(action=data, origin=Web.control.rc, **self.actions_kwargs)
        except ValueError:
            try:
                Web.control.reverse_component_state(component=data)
            except ValueError:
                Web.control.rc.logger.error(f'{who(self)}: Do not know what this is about: {data}')

    def log_request(self, code='-', size='-'):
        pass  # to not pollute terminal with each http request (happens every second - see META)
