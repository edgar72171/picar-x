import logging
from http.server import BaseHTTPRequestHandler


class CameraFeedHandler(BaseHTTPRequestHandler):
    output = None  # set externally before instantiation

    def do_GET(self):
        self.send_response(200)
        self.send_header('Age', str(0))
        self.send_header('Cache-Control', 'no-cache, private')
        self.send_header('Pragma', 'no-cache')
        self.send_header('Content-Type', 'multipart/x-mixed-replace; boundary=FRAME')
        self.end_headers()
        try:
            while True:
                with self.output.condition:
                    self.output.condition.wait()
                    frame = self.output.frame
                self.wfile.write(b'--FRAME\r\n')
                self.send_header('Content-Type', 'image/jpeg')
                self.send_header('Content-Length', str(len(frame)))
                self.end_headers()
                self.wfile.write(frame)
                self.wfile.write(b'\r\n')
        except Exception as e:
            logging.warning(f'Removed streaming client {self.client_address}: {str(e)}')
