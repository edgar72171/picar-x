# Pi Car-x AI
AI for SunFounder PiCar-X.

Quick Links:
- [PiCar-X AI powered RC](#Pi-Car-x-AI)
  - [State](#State)
  - [Architecture](#Architecture)
    - [AI](#ai)
    - [SW](#sw)
    - [HW](#hw)
  - [Setup](#setup)
  - [Usage](#usage)
  - [Contribute](#contribute)
  - [Contact me](#contact)

# State
barely working / work in progress

# Architecture
## AI
Asynchronous event-based decision-making layers:
* SELF PRESERVATION: Sensor > Control > Actuator
* REMOTE CONTROL: Web > Control2 > Actuator
* TASK PERFORMING: Task+Sensor > Control1 > Actuator
* SELF CONTROL: State > Control2 > Task 

## SW
[PI OS](https://www.raspberrypi.com/for-home/)
Python3.11+, multi-thread, HTML, CSS, JSCRIPT hobby project on .
PiCarAI

## HW
[PiCar-X](https://www.sunfounder.com/collections/main-products/products/picar-x) is [SunFounder's](https://www.sunfounder.com) car that is build around the [Raspberry Pi](https://www.raspberrypi.org).

# Setup
* PI OS
  * Wi-Fi hot-spot for RC
    * set it to cennect automatically
    * remove any onther WiFi connection

* setup.sh
   * requirements.txt
   * /etc/rc.local

## Usage
```python
cd ~/git/picarai
sudo python3 main.py
```

## Contribute
Branch, improve, pull request.
Feel free to be apart.

## Contact
edgar 7217@@outlook...com
