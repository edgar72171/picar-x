#!/bin/sh

# FIXME: USER,HOME,IP address dependency on RPI setup

if ping -c 1 -t 1 192.168.1.171 ; then
  ip=192.168.1.171
else
  ip=10.42.0.1
fi

ssh metro@${ip} "sudo pkill -f -9 \"python3 ./main.py\""
rsync -e 'ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null'  -La --exclude '.*' --exclude '__pycache__' --exclude 'venv' --delete '.' metro@${ip}:'/home/metro/git/picarai' && ssh metro@${ip} "cd /home/metro/git/picarai && sudo ./main.py -v --stop --start"
