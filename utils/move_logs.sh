#!/bin/sh

if ping -c 1 -t 1 192.168.1.171 ; then
  ip=192.168.1.171
else
  ip=10.42.0.1
fi

ssh metro@${ip} "sudo cp /root/picarai/picarai.log /home/metro && sudo chown metro:metro /home/metro/picarai.log" && scp metro@${ip}:/home/metro/picarai.log . && ssh metro@${ip} "sudo rm /root/picarai/picarai.log"
