#!/bin/bash
# https://www.laskakit.cz/sunfounder-roboticke-auto-picar-x-ai--stavebnice/?gad_source=1&gclid=CjwKCAjw_e2wBhAEEiwAyFFFo9zZRklrvaygQ1QuZ1CXqAKamXs5gDMXQJD0eul5Ksuz3yWhh3fyCBoCBGkQAvD_BwE


# TODO: move to setup.py?

# HAT
sudo apt install git python3-pip python3-setuptools python3-smbus
mkdir ~/git ; cd ~/git
git clone https://github.com/sunfounder/robot-hat.git
cd robot-hat
sudo python3 setup.py install
# FIXME: patch /usr/local/lib/python3.11/dist-packages/robot_hat-1.1.0-py3.11.egg/robot_hat/music.py user_name = os.getlogin() > user_name = 'metro'

cd ~/git
git clone -b picamera2 https://github.com/sunfounder/vilib.git
cd vilib
sudo python3 install.py

cd ~/git
git clone -b v2.0 https://github.com/sunfounder/picrawler.git
cd picrawler
sudo python3 setup.py install
sudo bash i2samp.sh

cd ~/git
git clone -b v2.0 https://github.com/sunfounder/picar-x.git
cd picar-x
sudo python3 setup.py install
sudo bash i2samp.sh

sudo apt install python3-daemon
sudo apt install espeak  # needed?

# start picarai after boot
# FIXME: remove last line from /etc/rc.local
sudo sh -c 'echo -e "cd /home/metro/git/picarai && /usr/bin/python3 ./main.py --start\nexit 0" >> /etc/rc.local'

# TODO:  replace os.getlogin() by "metro" in /usr/local/lib/python3.11/dist-packages/robot_hat-1.1.0-py3.11.egg/robot_hat/music.py
