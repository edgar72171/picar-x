from abc import ABC, abstractmethod

from actuators.action import Action
from actuators.actuator import Actuator
from config import Config
from lib.utils import who_long

try:
    from robot_hat import Servo, PWM, Pin  # noqa
except ModuleNotFoundError:
    from fakes.servo import Servo, PWM, Pin


class HeadAction(Action, ABC):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def execute(self, actuator) -> None:
        self.logger.debug(f'OLD {who_long(actuator)} -> {who_long(self)}: x={actuator.x} y={actuator.y}  {self.justification}')
        self.set(actuator)
        self.logger.debug(f'NEW {who_long(actuator)} -> {who_long(self)}: x={actuator.x} y={actuator.y}  {self.justification}')
        actuator.x_servo.angle(angle=Config.HEAD_CENTER_X_DEFAULT - actuator.x)
        actuator.y_servo.angle(angle=Config.HEAD_CENTER_Y_DEFAULT - actuator.y)

    @abstractmethod
    def set(self, actuator: Actuator) -> None:
        pass


class Head(Actuator):
    """
        Allows robot to turn "head" with camera left, right, top, down, center
    """
    def __init__(self) -> None:
        super().__init__(accepts=HeadAction)
        self.x: int = 0
        self.y: int = 0
        self.x_min: int = -70
        self.x_max: int = 70
        self.y_min: int = -30
        self.y_max: int = 30
        self.x_servo = Servo(PWM('P1'))
        self.y_servo = Servo(PWM('P0'))
        self.actions = {'⌜': LookLeftUp, '⎺': LookCenterUp, '⌝': LookRightUp,
                        '⎸': LookLeftCenter, '·': LookCenterCenter, '⎹': LookRightCenter,
                        '⌞': LookLeftDown, '⎽': LookCenterDown, '⌟': LookRightDown}


class LookLeftUp(HeadAction):
    def set(self, actuator: Head) -> None:
        actuator.x, actuator.y = actuator.x_min, actuator.y_max


class LookCenterUp(HeadAction):
    def set(self, actuator: Head) -> None:
        actuator.x, actuator.y = 0, actuator.y_max


class LookRightUp(HeadAction):
    def set(self, actuator: Head) -> None:
        actuator.x, actuator.y = actuator.x_max, actuator.y_max


class LookLeftCenter(HeadAction):
    def set(self, actuator: Head) -> None:
        actuator.x, actuator.y = actuator.x_min, 0


class LookCenterCenter(HeadAction):
    def set(self, actuator: Head) -> None:
        actuator.x, actuator.y = 0, 0


class LookRightCenter(HeadAction):
    def set(self, actuator: Head) -> None:
        actuator.x, actuator.y = actuator.x_max, 0


class LookLeftDown(HeadAction):
    def set(self, actuator: Head) -> None:
        actuator.x, actuator.y = actuator.x_min, actuator.y_min


class LookCenterDown(HeadAction):
    def set(self, actuator: Head) -> None:
        actuator.x, actuator.y = 0, actuator.y_min


class LookRightDown(HeadAction):
    def set(self, actuator: Head) -> None:
        actuator.x, actuator.y = actuator.x_max, actuator.y_min
