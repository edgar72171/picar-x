import time
from abc import ABC, abstractmethod
from actuators.action import Action
from actuators.actuator import Actuator

from config import Config
from lib.utils import who_long
from hw.motor2 import Motor

try:
    from robot_hat import Servo, PWM, Pin  # noqa
except ModuleNotFoundError:
    from fakes.servo import Servo, PWM, Pin


class EngineAction(Action, ABC):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def execute(self, actuator) -> None:
        self.logger.debug(f'OLD {who_long(actuator)} -> {who_long(self)}: speed={actuator.speed} angle={actuator.angle}  {self.justification}')

        self.set(actuator)
        self.logger.debug(f'NEW {who_long(actuator)} -> {who_long(self)}: speed={actuator.speed} angle={actuator.angle}  {self.justification}')
        actuator.motors.wheel(speed=-actuator.speed)
        actuator.direction.angle(angle=Config.STEER_DIRECTION_DEFAULT + actuator.angle)

    @abstractmethod
    def set(self, actuator: Actuator) -> None:
        pass


class Engine(Actuator):
    """
        Allows robot to perform action which will move it from one location to another: MoveForward, TurnLeft, Stop ...
        * Two motors + steering servo togather participate on the moving action.
        * All motor actions are performed exclusively on the same thread to ensure serialisation / prioritisation.
    """
    def __init__(self) -> None:
        super().__init__(accepts=EngineAction)
        self.speed = 0
        self.angle = 0
        self.latest_action = None
        self.motors = Motor()
        self.direction = Servo(PWM('P2'))
        self.actions = {'◀️': TurnLeft,
                        '↔': TurnCenter,
                        '▶': TurnRight,
                        '▲': MoveForward,
                        '▼': MoveBackwards,
                        '⏹': Stop,
                        '⏫': SpeedUp,
                        '⏬': SpeedDown,
                        '🔄': ResetMcu}
        self.reset_mcu()  # needed for motor function

    @staticmethod
    def reset_mcu():
        mcu_reset = Pin('MCURST')
        mcu_reset.off()
        time.sleep(0.001)
        mcu_reset.on()
        time.sleep(0.01)


class TurnLeft(EngineAction):
    def set(self, actuator: Engine) -> None:
        actuator.angle = Config.STEER_TURN_DEFAULT


class TurnCenter(EngineAction):
    def set(self, actuator: Engine) -> None:
        actuator.angle = 0


class TurnRight(EngineAction):
    def set(self, actuator: Engine) -> None:
        actuator.angle = -Config.STEER_TURN_DEFAULT


class MoveForward(EngineAction):
    def set(self, actuator: Engine) -> None:
        actuator.speed = Config.ENGINE_SPEED_DEFAULT


class MoveBackwards(EngineAction):
    def set(self, actuator: Engine) -> None:
        actuator.speed = -Config.ENGINE_SPEED_DEFAULT


class Stop(EngineAction):
    def set(self, actuator: Engine) -> None:
        actuator.speed = 0


class SpeedUp(EngineAction):
    def set(self, actuator: Engine) -> None:
        actuator.speed += Config.ENGINE_SPEED_DEFAULT


class SpeedDown(EngineAction):
    def set(self, actuator: Engine) -> None:
        actuator.speed -= Config.ENGINE_SPEED_DEFAULT


class ResetMcu(EngineAction):
    def set(self, actuator: Engine) -> None:
        actuator.reset_mcu()
