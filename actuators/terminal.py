from abc import ABC
from actuators.action import Action
from actuators.actuator import Actuator
from lib.unix import run


class TerminalAction(Action, ABC):
    pass


class Terminal(Actuator):
    def __init__(self) -> None:
        super().__init__(accepts=TerminalAction)
        self.actions = {'⏼': ShutDown,
                        '🔄': Reboot}


class ShutDown(TerminalAction):
    def execute(self, actuator: Terminal) -> None:
        run(['sudo', '/usr/sbin/shutdown'])


class Reboot(TerminalAction):
    def execute(self, actuator: Terminal) -> None:
        run(['sudo', '/usr/sbin/reboot'])
