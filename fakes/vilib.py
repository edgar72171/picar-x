from fakes.fake import Fake


class Vilib(Fake):
    @staticmethod
    def camera_start(vflip=False, hflip=False) -> None:
        pass

    @staticmethod
    def camera_close() -> None:
        pass

    @staticmethod
    def display(local=False, web=True) -> None:
        pass

    @staticmethod
    def take_photo(file_name: str, path: str) -> None:
        pass
