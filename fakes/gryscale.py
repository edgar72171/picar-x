from fakes.fake import Fake


class Grayscale_Module(Fake):
    def __init__(self, _, __, ___, reference: (float, float, float)):
        super().__init__()

    def reference(self, reference: (float, float, float)):
        pass

    @staticmethod
    def read():
        return 0.0, 0.0, 0.0
