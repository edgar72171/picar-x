from fakes.fake import Fake


class Picamera2(Fake):
    def configure(self, _: any):
        pass

    def create_video_configuration(self, main: dict) -> any:
        return None

    def start_recording(self, _: any, __: any) -> None:
        pass

    def stop_recording(self) -> None:
        pass
