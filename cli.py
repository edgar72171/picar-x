import argparse
import atexit
import daemon  # pip3 install python-daemon [https://www.python.org/dev/peps/pep-3143/]
import sys
import time
import os
import signal

from controls.control import Control
from lib.cli import CLI
from lib import utils
from config import Config


class Terminal(CLI):
    """ Any exception here is fatal and goes directly into terminal (except the daemonize method which detaches from the terminal > logs & rethrow exceptions). """
    daemon_running = 0, f'✅{Config.NAME} is running'
    daemon_not_running = 1, f'🟥{Config.NAME} is not running'
    daemon_dead = 2, f'💀{Config.NAME} is dead'
    daemon_permission_error = 3, f'🔒{Config.NAME} permission error'
    daemon_is_already_running = 4, f'⛔Another {Config.NAME} is already running'

    def __init__(self, log_path):
        super().__init__(log_path)
        self.parser = argparse.ArgumentParser(description='TODO', formatter_class=argparse.RawTextHelpFormatter, epilog=f'''
TODO
''')
        self.parser.add_argument('-d', '--debug', help=f'print debug information', action='store_true')
        self.parser.add_argument('-v', '--verbose', help=f'sets file logger verbosity ({self.logger.file_handler.baseFilename} has always full output)', action='count', default=0)

        self.parser.add_argument('-s', '--status', help='Return 0 if running, 1 if not. Terminal option, no point in combining with options below.', action='store_true')
        self.parser.add_argument('--stop', help=f'Stop daemonized {Config.NAME}', action='store_true')
        self.parser.add_argument('--start', help=f'Daemonize {Config.NAME} and start scheduling', action='store_true')  # Blocking until demon lives, yet detached from terminal

    @staticmethod
    def debug():
        for name, value in os.environ.items():
            print("{0}: {1}".format(name, value))

    def daemonize(self) -> None:
        try:
            def die_gracefully() -> None:
                self.logger.debug('Dyeing gracefully.')
                control.stop_main_loop()
                self.logger.debug(f'Removing: {Config.PID_FILE_PATH}')
                os.remove(Config.PID_FILE_PATH)  # --stop is waiting for this file to be removed
                self.logger.debug('Last record.')

            with daemon.DaemonContext(files_preserve=[self.logger.file_handler.stream.fileno()],  # other file handlers are closed
                                      working_directory=os.path.abspath(os.curdir),
                                      signal_map={signal.SIGTERM: 'terminate'}):  # SIGTERM needs to be handled in order to atexit work to die_gracefully
                self.logger.debug(f'daemonized')
                with open(Config.PID_FILE_PATH, 'w') as f:
                    f.write(str(os.getpid()))
                atexit.register(die_gracefully)
                control = Control()
                control.main_loop()  # blocking until alive

        except Exception as ex:
            self.logger.logger.exception(ex)  # any exception here is terminal > better logg forensics

    @staticmethod
    def send_daemon(signal_: int) -> (int, str):
        """Sends running daemon a given signal. Returns result whether it succeeded or why not."""
        try:
            with open(Config.PID_FILE_PATH, 'r') as f:
                pid = int(f.read().strip())
                os.kill(pid, signal_)
        except FileNotFoundError:
            return Terminal.daemon_not_running
        except PermissionError:
            return Terminal.daemon_permission_error
        except OSError:
            return Terminal.daemon_dead
        else:
            return Terminal.daemon_running

    def status(self) -> None:
        result, output = self.send_daemon(signal_=0)  # SIGNAL 0 is ignored by daemon, but fails if not delivered.
        self.logger.logger.info(f'status: {output}')
        sys.exit(result)

    def stop(self) -> None:
        result, output = self.send_daemon(signal_=signal.SIGTERM)  # handled by daemon to graceful terminate
        if result != 0:
            self.logger.logger.error(f'status: {output}')
            return

        assert utils.wait_for_callable(lambda: os.path.exists(Config.PID_FILE_PATH), expected_value=False, timeout=60, period=1)  # wait for daemon to die gracefully (remove its PID file)

    def start(self) -> None:
        status = self.send_daemon(signal_=0)  # SIGNAL 0 is ignored by daemon, but fails if not delivered.
        if status == Terminal.daemon_running:
            status = Terminal.daemon_is_already_running

        # terminal failures
        if status in (Terminal.daemon_is_already_running, Terminal.daemon_permission_error):
            result, output = status
            self.logger.logger.error(f'start: {output}')
            sys.exit(result)

        self.logger.debug('starting')
        if os.fork():
            time.sleep(Config.STARTING_TIME)  # PARENT process: Wait for CHILD process to start the daemon. Anything is fatal during daemons init.
            result, output = self.send_daemon(signal_=0)  # SIGNAL 0 is ignored by daemon, but fails if not delivered.
            self.logger.logger.error(f'start: {output}')
            sys.exit(result)

        self.daemonize()  # CHILD process:  blocking until daemon alive - but detached from terminal
