import io
import time
from threading import Condition

try:
    import cv2
    from picamera2 import Picamera2
    from picamera2.encoders import MJPEGEncoder  # TODO: check CPU load H264Encoder
    from picamera2.outputs import FileOutput
except ModuleNotFoundError:
    from fakes.vilib import Vilib
    from fakes.picamera import Picamera2


from sensors.sensor import Sensor
from controls.rc.web.handlers.camera import CameraFeedHandler


class Camera(Sensor):
    def __init__(self, period: float, control, store_last_values=5) -> None:
        super().__init__(period, store_last_values)
        self._control = control
        self.picam2 = Picamera2()
        self.picam2.configure(self.picam2.create_video_configuration(main={"size": (640, 480)}))

    def read_raw_value(self) -> any:
        if not CameraFeedHandler.output:
            CameraFeedHandler.output = StreamingOutput()  # FIXME: pass self._control for image processing
            self.picam2.start_recording(MJPEGEncoder(), FileOutput(CameraFeedHandler.output))

    def process_raw_value(self, raw) -> None:
        # TODO: image processing
        pass

    def jpg(self):
        # FIXME: give array[0]
        return cv2.imencode('.jpg', self.read_raw_value())[1].tobytes()

    def stop(self):
        print('claiming to stop camera')  # FIXME: camera was not stopped?
        self.picam2.stop_recording()
        # CameraFeedHandler.output = None
        # self.picam2.close()
        super().stop()


class StreamingOutput(io.BufferedIOBase):  # TODO: Trigger some image processing here?
    def __init__(self):
        self.frame = None
        self.condition = Condition()

    def write(self, buf):
        with self.condition:
            self.frame = buf
            self.condition.notify_all()
