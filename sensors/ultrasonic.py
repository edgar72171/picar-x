# TODO: from statistic import mode
from sensors.sensor import SensorControlFaulty
from lib.utils import who_long


try:
    from robot_hat import Ultrasonic as Ultrasonic_, Pin  # noqa
except ModuleNotFoundError:
    from fakes.ultrasonic import Ultrasonic as Ultrasonic_, Pin


class Ultrasonic(SensorControlFaulty):
    def __init__(self, **kwargs):  # FIXME: needed?
        super().__init__(**kwargs)
        self._ultrasonic = Ultrasonic_(trig=Pin('D2'),
                                       echo=Pin('D3'))

    def read_raw_value(self) -> [float, None]:
        self.logger.debug(f'Reading...{who_long(self)}')
        data = self._ultrasonic.read(times=1)
        self.logger.debug(f'...{data}')
        if data > 0:
            return data
        else:
            self.logger.debug(f'Returning invalid value: {data} as None')
            return None

    @property
    def state(self) -> str:
        return f'{super().state}cm'
