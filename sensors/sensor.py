import statistics
from abc import ABC, abstractmethod

from collections import deque
from lib.threading2 import LoggingExceptionsThread
from lib.utils import who
from controls.base import ComponentPeriod


class Sensor(ComponentPeriod, LoggingExceptionsThread, ABC):
    """
        Sensors job is to periodically:
         * read raw values from sensor
         * store NON-NULL raw values
         * on demand give the latest (processed) sensor value
    """
    def __init__(self, period: float, store_last_values: int = 10) -> None:
        ComponentPeriod.__init__(self, period)
        LoggingExceptionsThread.__init__(self)
        self.raw_values = deque(maxlen=store_last_values)
        self.values = deque(maxlen=store_last_values)

    @property
    def value(self) -> [any, None]:
        # return median of last 5 values
        self.logger.debug(f'{who(self)} Accessing latest value: {self.values[-1] if self.values else None} ({self.values})')
        return self.values[-1] if self.values else None

    def iterate(self) -> None:
        """ Sensor reading and data processing iteration (which gets periodically called while this thread lives). """
        self.logger.debug(f'Sensor: {who(self)} Iterating started.')
        raw_value = self.read_raw_value()
        self.logger.debug(f'Sensor: {who(self)} Reading finished ({raw_value}).')

        if raw_value is None:
            self.logger.debug(f'Sensor: {who(self)} Ignoring invalid value: ({raw_value}).')
            return

        self.raw_values.append(raw_value)
        self.logger.debug(f'Sensor: {who(self)} Processing started ({raw_value}).')
        self.process_raw_value(raw_value)

    @abstractmethod
    def read_raw_value(self) -> any:
        pass

    def process_raw_value(self, raw) -> None:
        self.values.append(raw)

    def stop(self):
        super().stop()
        self.logger.debug(f'{who(self)}\nraw_values={self.raw_values}\nvalues={self.values}')

    @property
    def state(self) -> str:
        return f'{super().state} {self.value}'


class SensorControl(Sensor, ABC):
    """ Sensor + ask control to process obtained value immediately when obtained to create/perform some (likely EMERGENCY priority) actions for actuators. """
    def __init__(self, period: float, control, store_last_values: int = 10) -> None:
        super().__init__(period, store_last_values)
        self._control = control

    def process_raw_value(self, raw) -> None:
        super().process_raw_value(raw)
        self._control.process_data(self)


class SensorControlFaulty(SensorControl, ABC):
    """
    Extension of SensorControl with extra processing of values to mitigate faulty sensors:
     * raw values too far from standard deviation are ignored during processing.
     * value is given as median of n-latest values instead of the very latest.
    """
    def process_raw_value(self, raw) -> None:
        if len(self.raw_values) != self.raw_values.maxlen:
            self.logger.debug(f'Not enough raw values to discard anything: {len(self.raw_values)} != {self.raw_values.maxlen}')
        else:
            median = statistics.median(self.raw_values)
            standard_deviation = statistics.stdev(self.raw_values)
            if not (raw - 2 * standard_deviation < median < raw + 2 * standard_deviation):  # FIXME: parametrise
                self.logger.debug(f'Discarding {raw=} value as too far from {median=} ({standard_deviation=}) \n{self.raw_values}')
                return
            else:
                self.logger.debug(f'Accepting {raw=} value as within expected 2*{standard_deviation=} from {median=} \n{self.raw_values=}\n{self.values=}')
        super().process_raw_value(raw)

    @property
    def value(self) -> [any, None]:
        if not self.values:
            return None

        latest_values_count = min(len(self.values), 5)  # FIXME: parametrise
        median = statistics.median(list(self.values)[-latest_values_count:])
        return median
