from config import Config
from sensors.sensor import SensorControl
from lib.utils import who_long

try:
    from robot_hat import ADC, Grayscale_Module  # noqa  # FIXME: needed?
except ModuleNotFoundError:
    from fakes.adc import ADC
    from fakes.gryscale import Grayscale_Module


class Grayscale(SensorControl):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.grayscale = Grayscale_Module('A0', 'A1', 'A2', reference=None)  #Config.DEFAULT_LINE_REFERENCE)

    def read_raw_value(self) -> (float, float, float):
        return self.grayscale.get_grayscale_data()

    def process_raw_value(self, raw) -> None:
        # TODO
        self.values.append(raw)

#    def get_line_status(self,gm_val_list):
#        return self.grayscale.read_status(gm_val_list)
