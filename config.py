class Config:
    """ Storage for an external environment configurations / dependencies. """
    NAME = 'picarai'
    PID_FILE_PATH = f'/var/run/{NAME}.pid'
    STARTING_TIME = 2                               # seconds of daemon early starting phase (when to expect fatal failures)

    RC_AP_HTTP_SERVER = ('10.42.0.1', 81)           # Access Point
    RC_AP_STREAM_SERVER = ('10.42.0.1', 82)         # Access Point
    RC_WLAN_HTTP_SERVER = ('192.168.1.171', 81)     # Wlan0
    RC_WLAN_STREAM_SERVER = ('192.168.1.171', 82)   # Wlan0
    LOCAL_RC_HTTP_SERVER = ('127.0.0.1', 81)        # fake for development outside PI FIXME: RC_HTTP_SERVER_LOCAL

    NORMAL_VOLTAGE = 4.8  # FIXME: VOLTAGE_NORMAL
    LOW_VOLTAGE = 4.5
    VERY_LOW_VOLTAGE = 4

    ENGINE_SPEED_DEFAULT = 25
    STEER_TURN_DEFAULT = -16
    STEER_DIRECTION_DEFAULT = -77
    HEAD_CENTER_X_DEFAULT = 60
    HEAD_CENTER_Y_DEFAULT = -72

    DEFAULT_LINE_REFERENCE = [1000.0, 1000.0, 1000.0]
