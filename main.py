#!/usr/bin/env python3

import sys
from pathlib import Path

from cli import Terminal


def main() -> int:
    t = Terminal(log_path=f'{Path.home()}/{Path.cwd().stem}/{Path.cwd().stem}.log')
    t.logger.logger.info(f'Logging here: {t.logger.file_handler.baseFilename}')
    any(t.execute_args())
    return 0


if __name__ == '__main__':
    sys.exit(main())
