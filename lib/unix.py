import subprocess
import logging
import random


def run(cmd, timeout=60) -> tuple:
    """ Execution of commands with finite output. Blocking until execution is finished. """
    with subprocess.Popen(cmd, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True) as p:
        try:
            outs, errs = p.communicate(timeout=timeout)
        except TimeoutError:
            p.kill()
            outs, errs = p.communicate()
        return p.poll(), outs, errs


def run_none_blocking(cmd) -> tuple:
    """ Same as run but non-blocking. """
    with subprocess.Popen(cmd, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True) as p:
        status = p.poll()
        outs = p.stdout.readline().strip()
        errs = p.stderr.readline().strip()
        while outs or errs or status is None:
            yield status, outs, errs
            status = p.poll()
            outs = p.stdout.readline().strip()
            errs = p.stderr.readline().strip()


def log_run(cmd, logger=logging.getLogger(__name__)) -> int:
    id_ = random.randint(0, 1000000)
    logger.debug('Start run=%d: %s' % (id_, str(cmd)))

    status = None
    for status, out, err in run_none_blocking(cmd):
        if out:
            logger.debug('run=%d: %s' % (id_, out))
        if err:
            logger.error('run=%d: %s' % (id_, err))
    logger.log(level=logging.ERROR if status else logging.DEBUG, msg='Finished run=%d with status: %s' % (id_, str(status)))
    return status


def tests():
    def print2(g):
        for status, out, err in g:
            if status:
                print(status)
            if out:
                print(out)
            if err:
                print(err)

    result, out, err = run(['reboot'])
    print(f'{out}\n{err}\nresult: {result}')
    """
    result, out, err = run(['/bin/ls', '-l', '/'])
    print(f'{out}\n{err}\nresult: {result}')

    print2(run2(['sleep', '0']))                 # 0-line output
    print2(run2(['echo', '-n', 'ok']))           # 1-line output
    print2(run2(['/bin/ls', '-l', '/', ]))       # n-line output
    #print2(run2(['top', '-l', '1']))             # n-line output - timed # FIXME
    print2(run2(['/bin/ls', '-l', '/xxx', ]))    # fail
    """


if __name__ == '__main__':  # tests
    tests()
